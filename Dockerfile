FROM node:16
WORKDIR /app
ARG NEXT_PUBLIC_API_URL
ENV NEXT_PUBLIC_API_URL=NEXT_PUBLIC_API_URL
RUN apt-get -y update
RUN apt-get -y install git
COPY package.json yarn.lock /app/
RUN yarn cache clean
RUN yarn install --network-concurrency 1

COPY . /app
RUN yarn build
CMD [ "yarn", "start" ]

