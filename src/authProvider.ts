import { AuthBindings } from "@refinedev/core";
import nookies from "nookies";
import Axios, { AxiosInstance } from 'axios'

export const authProvider = (apiClient: AxiosInstance = Axios.create()): AuthBindings => {
  apiClient.interceptors.request.use((config) => {
    let token = nookies.get()["auth"];
    if (config.params?._ctx) { // if context is pass to params
      token = nookies.get(config.params._ctx)["auth"];
    }
    if (token) {
      config.headers.Authorization = token
    }
    return config
  })
  return {
    login: async ({ email, username, password, remember }) => {
      // Suppose we actually send a request to the back end here.
      const { data: loginResult } = await apiClient.post('/users/login', {
        email,
        password
      })

      if (loginResult) {
        const token = loginResult.id
        nookies.set(null, "auth", token, {
          maxAge: 30 * 24 * 60 * 60,
          path: "/",
        });
        return {
          success: true,
          redirectTo: "/",
        };
      }

      return {
        success: false,
        error: {
          name: "LoginError",
          message: "Invalid username or password",
        },
      };
    },
    logout: async () => {
      nookies.destroy(null, "auth");
      return {
        success: true,
        redirectTo: "/login",
      };
    },
    check: async (ctx: any) => {
      const cookies = nookies.get(ctx);
      if (cookies["auth"]) {
        return {
          authenticated: true,
        };
      }

      return {
        authenticated: false,
        logout: true,
        redirectTo: "/login",
      };
    },
    getPermissions: async () => {
      // TODO
      // const auth = nookies.get()["auth"];
      // if (auth) {
      //   const parsedUser = JSON.parse(auth);
      //   return parsedUser.roles;
      // }
      return null;
    },
    getIdentity: async () => {
      const token = nookies.get()["auth"];
      if (token) {
        const {
          data: userInfo
        } = await apiClient.get('/users/me', {
          headers: {
            Authorization: token
          }
        })
        return userInfo
      }
      return null
    },
    onError: async (error) => {
      console.error(error);
      return { error };
    },
  }
};
